# This is meant to run inside Kopf (kopf run path/to/this/file.py)

import logging
import os
import re
import yaml

import kopf
import kubernetes
import requests


CAPI_CLUSTER_NAMESPACE_LABEL  = "capi-rancher-import.cluster-namespace"
CAPI_CLUSTER_NAME_LABEL       = "capi-rancher-import.cluster-name"

# This is just meant to test this controller in an environment
# which does not have direct access to Rancher k8s service
# for instance a dev environment where we can reach it only via
# an SSH tunnel.
# 
URL_REMAP_ENV_VAR = "URL_REMAP"
# example:
#   URL_REMAP="https://[^/]+/ https://localhost:9003/"


@kopf.on.startup()
def init_k8s(logger, **kwargs):
    if os.environ.get("CAPI_RANCHER_IMPORT_OUT_OF_POD", False):
        kubernetes.config.load_kube_config()
    else:
        kubernetes.config.load_incluster_config()

    required_env_vars = (
        'CATTLE_AGENT_KUSTOMIZATION_SOURCEREF_KIND',
        'CATTLE_AGENT_KUSTOMIZATION_SOURCEREF_NAME',
        'CATTLE_AGENT_KUSTOMIZATION_SOURCEREF_NAMESPACE',
        'CATTLE_AGENT_KUSTOMIZATION_PATH',
    )
    for e in required_env_vars:
        if e not in os.environ:
            logger.fatal(f"You need to set the all the following environment variables:\n{required_env_vars}")

    # silence the healthz probe logs
    logging.getLogger('aiohttp.access').setLevel(logging.WARNING)

    # check that the CRDs we need are defined
    api = kubernetes.client.CustomObjectsApi()
    missing_crds = []
    for crd_name in ('clusters.provisioning.cattle.io',
                     'clusterregistrationtokens.management.cattle.io',
                     'clusters.management.cattle.io',
                     'kustomizations.kustomize.toolkit.fluxcd.io',
            ):
        try:
            crd = api.get_cluster_custom_object("apiextensions.k8s.io", "v1", "customresourcedefinitions", crd_name)
            logging.info(f"found {crd_name} CRD")
        except kubernetes.client.exceptions.ApiException as e:
            if e.status == 404:
                missing_crds.append(crd_name)
            else:
                raise

    if missing_crds:
        msg = f"some CRDs we need aren't defined: {missing_crds}"
        logger.error(msg)
        raise Exception(msg)


@kopf.on.startup()
def configure(settings: kopf.OperatorSettings, **_):
    settings.persistence.progress_storage =  kopf.AnnotationsProgressStorage(prefix='status.capi-rancher-import')

    # disable the scanning of custom resources on the cluster - we don't need that (?)
   ## settings.scanning.disabled = True


def remap_rancher_server_url(url):
    if URL_REMAP_ENV_VAR not in os.environ:
        return url
    else:
        pattern, substitution = os.environ[URL_REMAP_ENV_VAR].split(' ')
        return re.sub(pattern, substitution, url)


def create_or_update_object(api, body):
    group, version = body['apiVersion'].split('/')
    plural = body['kind'].lower() + 's'
    name = body['metadata']['name']
    namespace = body['metadata']['namespace']

    try:
        current = api.get_namespaced_custom_object(group, version, namespace, plural, name)
        logging.debug(f"found existing {body['kind']} {namespace}/{name}, updating")
        body['metadata'].update({'resourceVersion': current['metadata']['resourceVersion']})
        return "updated", api.replace_namespaced_custom_object(group, version, namespace, plural, name, body)
    except kubernetes.client.exceptions.ApiException as e:
        if e.status == 404:
            logging.debug(f"no {body['kind']} {namespace}/{name} yet, creating")
            return "created", api.create_namespaced_custom_object(group, version, namespace, plural, body)
        else:
            raise


def cluster_registration_token_has_manifest(body, **_):
    try:
        return ('manifestUrl' in body['status'])
    except KeyError:
        return False




@kopf.on.resume('management.cattle.io','v3','ClusterRegistrationToken',
                when=cluster_registration_token_has_manifest)
@kopf.on.create('management.cattle.io','v3','ClusterRegistrationToken',
                when=cluster_registration_token_has_manifest)
@kopf.on.update('management.cattle.io','v3','ClusterRegistrationToken',  # FIXME: is this effective ?
                when=cluster_registration_token_has_manifest)
def handler_cluster_registration_token(body, **kwargs):
    rancher_mgmt_cluster_name = body['spec']['clusterName']
    logging.info(f"We have a ClusterRegistrationToken for {rancher_mgmt_cluster_name} management.cattle.io Cluster resource")

    # TODO when ClusterRegistrationToken has been processed, set an annotation
    # on it to not have to process this again (maybe Kopf has a way to help us do this?)

    api = kubernetes.client.CustomObjectsApi()

    # TODO, retry if we fail this:
    # TODO: be able to find this resource in a chosen namespace ? needed ?
    rancher_mgmt_cluster = api.get_cluster_custom_object(
        group="management.cattle.io",
        version="v3",
        plural="clusters",
        name=rancher_mgmt_cluster_name,
    )

    mgmt_cluster_annotations = rancher_mgmt_cluster['metadata']['annotations']

    logging.debug(f"management.cattle.io Cluster annotations: {mgmt_cluster_annotations}")

    try:
        prov_cluster_name = mgmt_cluster_annotations['objectset.rio.cattle.io/owner-name']
        prov_cluster_namespace = mgmt_cluster_annotations['objectset.rio.cattle.io/owner-namespace']
    except KeyError:
        logging.info(f"management.cattle.io Cluster '{rancher_mgmt_cluster['metadata']['name']}' "
                     f"does not have the 'objectset.rio.cattle.io/owner-' annotations we expect, ignoring")
        return

    logging.info(f"Will try to fetch provisioning.cattle.io Cluster resource: {prov_cluster_namespace}/{prov_cluster_name}")

    rancher_prov_cluster = api.get_namespaced_custom_object(
        group="provisioning.cattle.io",
        version="v1",
        plural="clusters",
        namespace=prov_cluster_namespace,
        name=prov_cluster_name,        
    )

    logging.debug(f"provisioning.cattle.io Cluster metadata: {rancher_prov_cluster['metadata']}")

    # Let's find our labels of interest  in the Rancher provisioning.cattle.io Cluster resource...

    rancher_prov_cluster_labels = rancher_prov_cluster['metadata'].get('labels', {})

    cluster_api_cluster_name = rancher_prov_cluster_labels.get(CAPI_CLUSTER_NAME_LABEL,None)
    cluster_api_cluster_namespace = (
        rancher_prov_cluster_labels.get(CAPI_CLUSTER_NAMESPACE_LABEL,
                                        rancher_prov_cluster['metadata']['namespace'])
    )
    # TODO, document that the namespace is optional

    if not cluster_api_cluster_name:
        logging.info(f"Rancher cluster {prov_cluster_namespace}/{prov_cluster_name} isn't labelled for importing a ClusterAPI cluster, ignoring")
        return
    else:
        logging.info(f"Let's import ClusterAPI cluster {cluster_api_cluster_namespace}/{cluster_api_cluster_name} into Rancher")
    
    # Fetching the manifests from Rancher

    manifest_url = remap_rancher_server_url(body.status['manifestUrl'])
    verify = (
        os.environ.get('REQUESTS_SSL_VERIFY','true').lower()
        in ['true', '1', 't', 'y', 'yes']
    )

    logging.info(f"Will fetch manifest from {manifest_url} ({verify=})")

    req = requests.get(manifest_url, verify=verify)

    logging.debug(f"Rancher manifest: {req.text}")

    rancher_cattle_agent_manifest = req.content

    # Build Flux Kustomization object to deploy these manifests

    # We rely on:
    #
    # * a kustomization.yml that Flux accesses via Git
    #   which has a skeleton for the Rancher cattle agent resources
    #   (which is in the )
    #
    # * TODO: a ConfigMap that contains a template for a FluxCD Kustomization
    #   object that (a) points to this kustomization, and that (b) we
    #   define with a strategic merge patch that overloads what is in
    #   this skeleton with the manifest downloaded from Rancher

    # TODO: the base definition for the Flux Kustomization
    # should be loaded from disk, which would be a file mapped from a ConfigMap
    flux_kustomization = yaml.safe_load(f'''
    apiVersion: kustomize.toolkit.fluxcd.io/v1
    kind: Kustomization
    metadata:
      name: PLACEHOLDER
      namespace: PLACEHOLDER
      labels:
        capi-rancher-import: "yes"
    spec:
      sourceRef:
        kind: {os.environ.get('CATTLE_AGENT_KUSTOMIZATION_SOURCEREF_KIND')}
        name: {os.environ.get('CATTLE_AGENT_KUSTOMIZATION_SOURCEREF_NAME')}
        namespace: {os.environ.get('CATTLE_AGENT_KUSTOMIZATION_SOURCEREF_NAMESPACE')}
      path: {os.environ.get('CATTLE_AGENT_KUSTOMIZATION_PATH')}
      interval: 1440m0s
      retryInterval: 1m
      prune: false
      wait: true
      images:
      - name: rancher/rancher-agent
        newName: docker.io/rancher/rancher-agent
    ''')

    # the Kustomization must be in the same namespace as the ClusterAPI
    # kubeConfig Secret, so we need to set its namespace

    flux_kustomization['metadata']['name'] = f"cattle-agent-{cluster_api_cluster_name}"
    flux_kustomization['metadata']['namespace'] = cluster_api_cluster_namespace
    flux_kustomization['metadata'].setdefault('labels',{})['capi-cluster-name'] = cluster_api_cluster_name

    # inject Rancher Manifest

    flux_kustomization['spec'].setdefault('patches',[])
    patches = flux_kustomization['spec']['patches']

    cattle_credentials_secret_name = ""

    for resource in yaml.load_all(rancher_cattle_agent_manifest, Loader=yaml.loader.SafeLoader):
        resource_kind = resource['kind']
        resource_name = resource['metadata']['name']
        patches.insert(0,{
            'target': {
                'kind': resource_kind,
                'name': resource_name,
            },
            'patch': yaml.dump(resource)
        })

        # remember the full name of the Secret cattle-credentials-* resource (see below why)
        if resource_kind == 'Secret' and re.match(r"cattle-credentials-.*", resource_name):
            cattle_credentials_secret_name = resource_name

    # the name of the Secret resource to patch in our skeleton is the name of the resource from the Rancher manifest
    # which contrarily to others has a dynamically generated name, which we cannot patch with a strategic merge patch
    patches.insert(0,{
        'target': {
            'kind': 'Secret',
            'name': 'cattle-credentials-placeholder',
        },
        'patch': yaml.dump([{"path":"/metadata/name","op":"replace","value": cattle_credentials_secret_name}])
    })

    # inject reference to the kubeConfig of our clusterAPI cluster

    flux_kustomization['spec']           \
        .setdefault('kubeConfig',{})     \
        .setdefault('secretRef',{})      \
        .update({'name': f"{cluster_api_cluster_name}-kubeconfig"})

    # make the Cluster provisioning.cattle.io object owner of our Kustomization
    kopf.append_owner_reference(flux_kustomization, owner=rancher_prov_cluster)

    logging.debug(f"Flux Kustomization:\n{yaml.dump(flux_kustomization)}\n")

    # create the Flux Kustomization
    action, flux_kustomization_res = create_or_update_object(api, flux_kustomization)
    
    logging.info(f"Flux Kustomization {cluster_api_cluster_namespace}/cattle-agent-{cluster_api_cluster_name} was "
                 f"{action} to deploy Rancher Cattle agent in remote CAPI cluster")
