#!/bin/bash
#
# This script will push to registry.gitlab.com an OCI registry artifact
# containing the content of 'cattle-kustomize' directory
#
# The artifact is pushed as:
#  oci://registry.gitlab.com/sylva-projects/sylva-elements/helm-charts/capi-rancher-import:<tag>
#
# The script accepts an optional parameter, which will be used as <tag> above.
# By default the current commit id will be used as <tag>.
#
# If run manually, the tool can be used after having preliminarily done
# a 'docker login registry.gitlab.com' with suitable credentials.
#
# Requirements:
# - flux (used to push the artifact)

set -eu
set -o pipefail

BASE_DIR="$(realpath $(dirname $0)/..)"
OCI_REGISTRY_ARTIFACT=oci://registry.gitlab.com/sylva-projects/sylva-elements/helm-charts/capi-rancher-import/cattle-kustomize

artifact_source="$(git config --get remote.origin.url)"
artifact_revision="$(git branch --show-current)/$(git rev-parse HEAD)"
artifact_tag="${1:-$(git rev-parse --short HEAD)}"
artifact_path="./cattle-kustomize"

# if we run in a gitlab CI job, then we use the credentials provided by gitlab job environment
if [[ -n ${CI_REGISTRY_USER:-} ]]; then
    creds="--creds $CI_REGISTRY_USER:$CI_REGISTRY_PASSWORD"
fi

if [[ -n ${CI_REGISTRY_IMAGE:-} ]]; then
    OCI_REGISTRY_ARTIFACT="oci://$CI_REGISTRY_IMAGE/cattle-kustomize"
fi

flux push artifact $OCI_REGISTRY_ARTIFACT:$artifact_tag \
	--path=$artifact_path \
	--source=$artifact_source \
	--revision=$artifact_revision \
    ${creds:-}
