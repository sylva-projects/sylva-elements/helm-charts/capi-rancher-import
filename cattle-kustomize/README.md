Skeleton of resources to deploy Rancher Cattle Agent

Taken from a Rancher URL like https://rancher-server:8443/v3/import/<token>_<clustername>.yaml
emptied from all fields specific to a given cluster registration (tokens, fields, etc).

This is used by Flux Kustomizations dynamically built by CAPI Rancher Import
controller, which will patch it with real values for (tokens, settings, up to date image tags, etc.)
coming from the dynamically built manifests that Rancher creates to let us import a cluster.

